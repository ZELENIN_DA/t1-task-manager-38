package ru.t1.dzelenin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dzelenin.tm.api.repository.IProjectRepository;
import ru.t1.dzelenin.tm.api.service.IProjectService;
import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.marker.UnitCategory;
import ru.t1.dzelenin.tm.model.Project;
import ru.t1.dzelenin.tm.repository.ProjectRepository;

import static ru.t1.dzelenin.tm.constant.ProjectTestData.*;
import static ru.t1.dzelenin.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class ProjectServiceTest {
/*
    @NotNull
    IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    IProjectService projectService = new ProjectService(projectRepository);

    @Test
    public void add() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        projectService.add(USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1, projectService.findAll().get(0));
    }

    @Test
    public void addByUserId() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        projectService.add(USER1.getId(), USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1, projectService.findAll().get(0));
        Assert.assertEquals(USER1.getId(), projectService.findAll().get(0).getUserId());
    }

    @Test
    public void clearByUserId() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        projectService.add(USER1_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT_LIST, projectService.findAll());
        projectService.removeAll(USER2.getId());
        Assert.assertFalse(projectService.findAll().isEmpty());
        projectService.removeAll(USER1.getId());
        Assert.assertTrue(projectService.findAll().isEmpty());
        projectService.add(USER2_PROJECT1);
        projectService.removeAll(USER1.getId());
        Assert.assertFalse(projectService.findAll().isEmpty());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        projectService.add(PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT_LIST, projectService.findAll(USER1.getId()));
    }

    @Test
    public void findOneByIdByUserId() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        projectService.add(USER1_PROJECT1);
        projectService.add(USER2_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1, projectService.findOneById(USER1.getId(), USER1_PROJECT1.getId()));
        Assert.assertNotEquals(USER2_PROJECT1, projectService.findOneById(USER1.getId(), USER2_PROJECT1.getId()));
    }

    @Test
    public void removeByUserId() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        projectService.add(USER1_PROJECT1);
        projectService.add(USER2_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1, projectService.removeOne(USER1.getId(), USER1_PROJECT1));
        Assert.assertFalse(projectService.findAll().contains(USER1_PROJECT1));
        Assert.assertTrue(projectService.findAll().contains(USER2_PROJECT1));
    }

    @Test
    public void removeByIdByUserId() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        projectService.add(USER1_PROJECT1);
        projectService.add(USER2_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1, projectService.removeOneById(USER1.getId(), USER1_PROJECT1.getId()));
        Assert.assertFalse(projectService.findAll().contains(USER1_PROJECT1));
        Assert.assertTrue(projectService.findAll().contains(USER2_PROJECT1));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        projectService.add(USER1_PROJECT1);
        Assert.assertTrue(projectService.existsById(USER1_PROJECT1.getId()));
        Assert.assertFalse(projectService.existsById(USER2_PROJECT1.getId()));
    }

    @Test
    public void changeProjectStatusById() {
        projectService.add(USER1_PROJECT1);
        Assert.assertEquals(Status.NOT_STARTED, USER1_PROJECT1.getStatus());
        projectService.changeProjectStatusId(USER1_PROJECT1.getUserId(), USER1_PROJECT1.getId(), Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, USER1_PROJECT1.getStatus());
    }

    @Test
    public void createProjectName(){
        Assert.assertTrue(projectService.findAll().isEmpty());
        @NotNull Project project = projectService.create(USER1.getId(), "test_project");
        Assert.assertEquals(project, projectService.findOneById(project.getId()));
        Assert.assertEquals("test_project", project.getName());
        Assert.assertEquals(USER1.getId(), project.getUserId());
    }

    @Test
    public void createProjectNameDescription() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        @NotNull Project project = projectService.create(USER1.getId(), "test_project", "test_description");
        Assert.assertEquals(project, projectService.findOneById(project.getId()));
        Assert.assertEquals("test_project", project.getName());
        Assert.assertEquals("test_description", project.getDescription());
        Assert.assertEquals(USER1.getId(), project.getUserId());
    }

    @Test
    public void updateById() {
        Assert.assertTrue(projectService.findAll().isEmpty());
        @NotNull Project project = projectService.create(USER1.getId(), "test_project", "test_description");
        projectService.updateById(USER1.getId(), project.getId(), "new name", "new description");
        Assert.assertEquals("new name", project.getName());
        Assert.assertEquals("new description", project.getDescription());
    }
*/
}
