package ru.t1.dzelenin.tm.repository;

import org.jetbrains.annotations.NotNull;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.dzelenin.tm.enumerated.Role;
import ru.t1.dzelenin.tm.marker.UnitCategory;
import ru.t1.dzelenin.tm.model.User;
import ru.t1.dzelenin.tm.service.PropertyService;
import ru.t1.dzelenin.tm.util.HashUtil;

import static ru.t1.dzelenin.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {
/*
    @NotNull
    private final UserRepository userRepository = new UserRepository();

    @NotNull
    private final PropertyService propertyService = new PropertyService();

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @Test
    public void add() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER1);
        Assert.assertEquals(USER1, userRepository.findAll().get(0));
    }

    @Test
    public void addList() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER_LIST1);
        Assert.assertEquals(USER_LIST1, userRepository.findAll());
    }

    @Test
    public void set() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER_LIST1);
        userRepository.set(USER_LIST2);
        Assert.assertEquals(USER_LIST2, userRepository.findAll());
    }

    @Test
    public void clear() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER_LIST1);
        Assert.assertFalse(userRepository.findAll().isEmpty());
        userRepository.removeAll();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void removeById() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER1);
        Assert.assertFalse(userRepository.findAll().isEmpty());
        userRepository.removeOneById(USER1.getId());
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void existsById() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER1);
        Assert.assertTrue(userRepository.existsById(USER1.getId()));
        Assert.assertFalse(userRepository.existsById(USER2.getId()));
    }

    @Test
    public void isLoginExist() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER1);
        USER1.setLogin(UNIT_TEST_USER_LOGIN);
        Assert.assertTrue(userRepository.isLoginExist(USER1.getLogin()));
        Assert.assertFalse(userRepository.isLoginExist(UNIT_TEST_INCORRECT_LOGIN));
    }

    @Test
    public void isEmailExist() {
        Assert.assertTrue(userRepository.findAll().isEmpty());
        userRepository.add(USER1);
        USER1.setEmail("test@test.ru");
        Assert.assertTrue(userRepository.isMailExist(USER1.getEmail()));
        Assert.assertFalse(userRepository.isMailExist(UNIT_TEST_USER_EMAIL));
    }
*/
}
