package ru.t1.dzelenin.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.api.repository.IProjectRepository;
import ru.t1.dzelenin.tm.api.repository.IRepository;
import ru.t1.dzelenin.tm.api.repository.IUserOwnedRepository;
import ru.t1.dzelenin.tm.api.service.IConnectionService;
import ru.t1.dzelenin.tm.api.service.IProjectService;
import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dzelenin.tm.exception.entity.StatusEmptyException;
import ru.t1.dzelenin.tm.exception.field.*;
import ru.t1.dzelenin.tm.model.Project;
import ru.t1.dzelenin.tm.repository.ProjectRepository;

import java.sql.Connection;
import java.util.Comparator;
import java.util.List;

public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    public IProjectRepository getRepository(@NotNull Connection connection) {
        return new ProjectRepository(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project create(@NotNull final String userId, @NotNull final String name) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();
        @NotNull Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            project = repository.add(project);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();
        if (description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            project = repository.add(project);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project updateById(@NotNull final String userId,
                              @NotNull final String id,
                              @NotNull final String name,
                              @NotNull final String description) {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (name == null || name.isEmpty())
            throw new NameEmptyException();
        @NotNull final Project project = findOneById(userId, id);
        project.setName(name);
        project.setDescription(description);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            repository.update(project);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return project;
    }


    @NotNull
    @Override
    @SneakyThrows
    public Project changeProjectStatusId(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        @NotNull final Project project = findOneById(userId, id);
        project.setStatus(status);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            repository.update(project);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return project;
    }

    @Override
    @SneakyThrows
    public void removeAll(@NotNull final String userId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            repository.removeAll(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public long getCount(@NotNull final String userId) {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IProjectRepository repository = getRepository(connection);
            return repository.getCount();
        }
    }

}

