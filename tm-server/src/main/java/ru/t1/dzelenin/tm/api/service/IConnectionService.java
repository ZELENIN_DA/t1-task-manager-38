package ru.t1.dzelenin.tm.api.service;

import org.jetbrains.annotations.NotNull;

import java.sql.Connection;

public interface IConnectionService {

    @NotNull
    Connection getConnection();

}

