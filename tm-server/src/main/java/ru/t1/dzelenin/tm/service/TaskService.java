package ru.t1.dzelenin.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.dzelenin.tm.api.repository.IProjectRepository;
import ru.t1.dzelenin.tm.api.repository.ITaskRepository;
import ru.t1.dzelenin.tm.api.service.IConnectionService;
import ru.t1.dzelenin.tm.api.service.ITaskService;
import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.enumerated.TaskSort;
import ru.t1.dzelenin.tm.exception.entity.StatusEmptyException;
import ru.t1.dzelenin.tm.exception.entity.TaskNotFoundException;
import ru.t1.dzelenin.tm.exception.field.*;
import ru.t1.dzelenin.tm.model.Task;
import ru.t1.dzelenin.tm.repository.TaskRepository;

import java.sql.Connection;
import java.util.Collections;
import java.util.List;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    public ITaskRepository getRepository(@NotNull Connection connection) {
        return new TaskRepository(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task create(@NotNull final String userId, @NotNull final String name) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();
        @NotNull Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            task = repository.add(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }


    @Override
    public List<Task> findAll(@NotNull final String userId, @NotNull final TaskSort sort) {
        return null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task create(@NotNull final String userId, @NotNull final String name, @NotNull final String description) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (name.isEmpty()) throw new NameEmptyException();
        if (description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Task task = new Task();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            task = repository.add(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @Override
    @SneakyThrows
    public Task updateById(@NotNull final String userId,
                           @NotNull final String id,
                           @NotNull final String name,
                           @NotNull final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final Task task = findOneById(userId, id);
        task.setName(name);
        task.setDescription(description);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            repository.update(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task changeTaskStatusId(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status
    ) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (id.isEmpty()) throw new IdEmptyException();
        @NotNull final Task task = findOneById(userId, id);
        task.setStatus(status);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            repository.update(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId.isEmpty()) return Collections.emptyList();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final ITaskRepository repository = getRepository(connection);
            return repository.findAllByProjectId(userId, projectId);
        }
    }

    @Override
    @SneakyThrows
    public long getCount(@NotNull final String userId) {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final ITaskRepository repository = getRepository(connection);
            return repository.getCount();
        }
    }

}

