package ru.t1.dzelenin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.model.Project;

public interface IProjectService extends IUserOwnerService<Project> {

    @NotNull
    Project changeProjectStatusId(@NotNull String userId, @NotNull String id, @NotNull Status status);

    @NotNull
    Project create(@Nullable String userId, @NotNull String name, @NotNull String description);

    @NotNull
    Project create(@NotNull String userId, @NotNull String name);

    @NotNull
    Project updateById(@NotNull String userId,
                       @NotNull String id,
                       @NotNull String name,
                       @NotNull String description);

}



