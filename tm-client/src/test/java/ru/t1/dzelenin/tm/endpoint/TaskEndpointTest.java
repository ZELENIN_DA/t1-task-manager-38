package ru.t1.dzelenin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dzelenin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.dzelenin.tm.api.endpoint.IProjectEndpoint;
import ru.t1.dzelenin.tm.api.endpoint.ITaskEndpoint;
import ru.t1.dzelenin.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.dzelenin.tm.dto.request.task.*;
import ru.t1.dzelenin.tm.dto.request.user.UserLoginRequest;
import ru.t1.dzelenin.tm.dto.response.project.ProjectCreateResponse;
import ru.t1.dzelenin.tm.dto.response.task.*;
import ru.t1.dzelenin.tm.dto.response.user.UserLoginResponse;
import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.enumerated.TaskSort;
import ru.t1.dzelenin.tm.marker.IntegrationCategory;

import java.util.UUID;

@Category(IntegrationCategory.class)
public final class TaskEndpointTest {

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance();

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @Nullable
    private String userToken;

    @Before
    public void init() {
        @Nullable final UserLoginResponse loginResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        userToken = loginResponse.getToken();
    }

    @After
    public void clear() {
        taskEndpoint.clearTask(new TaskClearRequest(userToken));
    }

    @Test
    public void taskBindToProject() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.bindTaskToProject(
                new TaskBindToProjectRequest())
        );
        Assert.assertThrows(Exception.class, () -> taskEndpoint.bindTaskToProject(
                new TaskBindToProjectRequest(null, null, null))
        );
        Assert.assertThrows(Exception.class, () -> taskEndpoint.bindTaskToProject(
                new TaskBindToProjectRequest("wrongToken", null, null))
        );
        @NotNull final String testProjectName = "UnitTestProject_" + UUID.randomUUID().toString();
        @NotNull final String testTaskName = "UnitTestTask_" + UUID.randomUUID().toString();
        @Nullable ProjectCreateResponse projectResponse = projectEndpoint.createProject(
                new ProjectCreateRequest(userToken, testProjectName, "desc")
        );
        Assert.assertNotNull(projectResponse);
        Assert.assertNotNull(projectResponse.getProject());
        @NotNull final String projectId = projectResponse.getProject().getId();
        @Nullable TaskCreateResponse taskResponse = taskEndpoint.createTask(
                new TaskCreateRequest(userToken, testTaskName, "desc")
        );
        Assert.assertNotNull(taskResponse);
        Assert.assertNotNull(taskResponse.getTask());
        @NotNull final String taskId = taskResponse.getTask().getId();
        @Nullable TaskBindToProjectResponse bindResponse = taskEndpoint.bindTaskToProject(
                new TaskBindToProjectRequest(userToken, projectId, taskId)
        );
        Assert.assertNotNull(bindResponse);
        @Nullable final TaskShowByIdResponse getResponse = taskEndpoint.showTaskById(
                new TaskShowByIdRequest(userToken, taskId)
        );
        Assert.assertNotNull(getResponse);
        Assert.assertNotNull(getResponse.getTask());
        Assert.assertEquals(projectResponse.getProject().getId(), getResponse.getTask().getProjectId());
    }

    @Test
    public void taskChangeStatusById() {
        @NotNull final String testTaskName = "UnitTestTask_" + UUID.randomUUID().toString();
        Assert.assertThrows(Exception.class, () -> taskEndpoint.changeTaskStatusById(
                new TaskChangeStatusByIdRequest(null, null, null)
        ));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.changeTaskStatusById(
                new TaskChangeStatusByIdRequest("wrongToken", null, null)
        ));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.changeTaskStatusById(
                new TaskChangeStatusByIdRequest(userToken, "wrongTaskId", null)
        ));
        @Nullable TaskCreateResponse taskResponse = taskEndpoint.createTask(
                new TaskCreateRequest(userToken, testTaskName, "desc")
        );
        Assert.assertNotNull(taskResponse);
        Assert.assertNotNull(taskResponse.getTask());
        @Nullable TaskChangeStatusByIdResponse response = taskEndpoint.changeTaskStatusById(
                new TaskChangeStatusByIdRequest(userToken, taskResponse.getTask().getId(), Status.IN_PROGRESS)
        );
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTask());
        Assert.assertEquals(Status.IN_PROGRESS, response.getTask().getStatus());
    }

    @Test
    public void taskClear() {
        @NotNull final String testTaskName = "UnitTestTask_" + UUID.randomUUID().toString();
        Assert.assertThrows(Exception.class, () -> taskEndpoint.clearTask(
                new TaskClearRequest()
        ));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.clearTask(
                new TaskClearRequest(null)
        ));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.clearTask(
                new TaskClearRequest("")
        ));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.clearTask(
                new TaskClearRequest("wrongToken")
        ));
        @Nullable TaskCreateResponse createResponse = taskEndpoint.createTask(
                new TaskCreateRequest(userToken, testTaskName, "desc")
        );
        Assert.assertNotNull(createResponse);
        Assert.assertNotNull(createResponse.getTask());
        @Nullable TaskClearResponse clearResponse = taskEndpoint.clearTask(
                new TaskClearRequest(userToken)
        );
        Assert.assertNotNull(clearResponse);
    }

    @Test
    public void taskCreate() {
        @NotNull final String testTaskName = "UnitTestTask_" + UUID.randomUUID().toString();
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(
                new TaskCreateRequest(userToken, null, "1")
        ));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(
                new TaskCreateRequest(null, null, null)
        ));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(
                new TaskCreateRequest("wrongToken", null, null)
        ));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.createTask(
                new TaskCreateRequest("wrongToken", testTaskName, null)
        ));
        @Nullable TaskCreateResponse response = taskEndpoint.createTask(
                new TaskCreateRequest(userToken, testTaskName, "desc")
        );
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTask());
        @Nullable TaskShowByIdResponse getResponse = taskEndpoint.showTaskById(
                new TaskShowByIdRequest(userToken, response.getTask().getId())
        );
        Assert.assertNotNull(getResponse);
        Assert.assertNotNull(getResponse.getTask());
    }

    @Test
    public void taskListByProjectId() {
        @NotNull final String testTaskName1 = "UnitTestTask_" + UUID.randomUUID().toString();
        @NotNull final String testTaskName2 = "UnitTestTask_" + UUID.randomUUID().toString();
        @NotNull final String testProjectName = "UnitTestProject_" + UUID.randomUUID().toString();
        Assert.assertThrows(Exception.class, () -> taskEndpoint.listTaskByProjectId(
                new TaskListByProjectIdRequest(null, null)
        ));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.listTaskByProjectId(
                new TaskListByProjectIdRequest(null, null)
        ));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.listTaskByProjectId(
                new TaskListByProjectIdRequest("wrongToken", null)
        ));
        @Nullable ProjectCreateResponse projectResponse = projectEndpoint.createProject(
                new ProjectCreateRequest(userToken, testProjectName, "desc")
        );
        Assert.assertNotNull(projectResponse);
        Assert.assertNotNull(projectResponse.getProject());
        @NotNull final String projectId = projectResponse.getProject().getId();
        @Nullable TaskCreateResponse taskResponse1 = taskEndpoint.createTask(
                new TaskCreateRequest(userToken, testTaskName1, "desc")
        );
        Assert.assertNotNull(taskResponse1);
        Assert.assertNotNull(taskResponse1.getTask());
        @NotNull final String taskId = taskResponse1.getTask().getId();
        @Nullable TaskCreateResponse taskResponse2 = taskEndpoint.createTask(
                new TaskCreateRequest(userToken, testTaskName2, "desc")
        );
        Assert.assertNotNull(taskResponse2);
        Assert.assertNotNull(taskResponse2.getTask());
        @Nullable TaskBindToProjectResponse bindResponse = taskEndpoint.bindTaskToProject(
                new TaskBindToProjectRequest(userToken, projectId, taskId)
        );
        Assert.assertNotNull(bindResponse);
        @Nullable TaskListByProjectIdResponse response = taskEndpoint.listTaskByProjectId(
                new TaskListByProjectIdRequest(userToken, projectId)
        );
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTasks());
        Assert.assertEquals(response.getTasks().size(), 1);
        Assert.assertEquals(response.getTasks().get(0).getId(), taskId);
    }

    @Test
    public void taskList() {
        @NotNull final String testTaskName1 = "UnitTestTask_" + UUID.randomUUID().toString();
        @NotNull final String testTaskName2 = "UnitTestTask_" + UUID.randomUUID().toString();
        Assert.assertThrows(Exception.class, () -> taskEndpoint.listTask(
                new TaskListRequest(userToken, null)
        ));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.listTask(
                new TaskListRequest(null, null)
        ));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.listTask(
                new TaskListRequest("wrongToken", null)
        ));
        @Nullable TaskCreateResponse taskResponse1 = taskEndpoint.createTask(
                new TaskCreateRequest(userToken, testTaskName1, "desc")
        );
        Assert.assertNotNull(taskResponse1);
        Assert.assertNotNull(taskResponse1.getTask());
        @NotNull final String taskId = taskResponse1.getTask().getId();
        @Nullable TaskCreateResponse taskResponse2 = taskEndpoint.createTask(
                new TaskCreateRequest(userToken, testTaskName2, "desc")
        );
        Assert.assertNotNull(taskResponse2);
        Assert.assertNotNull(taskResponse2.getTask());
        @Nullable TaskListResponse response = taskEndpoint.listTask(
                new TaskListRequest(userToken, TaskSort.BY_NAME)
        );
        Assert.assertNotNull(response);
        Assert.assertEquals(response.getTasks().size(), 2);
        @Nullable TaskListResponse response2 = taskEndpoint.listTask(
                new TaskListRequest(userToken, TaskSort.BY_NAME)
        );
        Assert.assertNotNull(response2);
        Assert.assertEquals(response2.getTasks().size(), 2);
    }

    @Test
    public void taskRemoveById() {
        @NotNull final String testTaskName = "UnitTestTask_" + UUID.randomUUID().toString();
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskById(
                new TaskRemoveByIdRequest()
        ));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskById(
                new TaskRemoveByIdRequest(null, null)
        ));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.removeTaskById(
                new TaskRemoveByIdRequest("wrongToken", null)
        ));
        @Nullable TaskCreateResponse taskResponse = taskEndpoint.createTask(
                new TaskCreateRequest(userToken, testTaskName, "desc")
        );
        Assert.assertNotNull(taskResponse);
        Assert.assertNotNull(taskResponse.getTask());
        @Nullable TaskShowByIdResponse getByIdResponse = taskEndpoint.showTaskById(
                new TaskShowByIdRequest(userToken, taskResponse.getTask().getId())
        );
        Assert.assertNotNull(getByIdResponse);
        Assert.assertNotNull(getByIdResponse.getTask());
        @Nullable TaskRemoveByIdResponse removeResponse = taskEndpoint.removeTaskById(
                new TaskRemoveByIdRequest(userToken, taskResponse.getTask().getId())
        );
        Assert.assertNotNull(removeResponse);
        Assert.assertNotNull(removeResponse.getTask());
    }

    @Test
    public void taskUnbindFromProject() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.unbindTaskFromProject(
                new TaskUnbindFromProjectRequest())
        );
        Assert.assertThrows(Exception.class, () -> taskEndpoint.unbindTaskFromProject(
                new TaskUnbindFromProjectRequest(null, null, null))
        );
        Assert.assertThrows(Exception.class, () -> taskEndpoint.unbindTaskFromProject(
                new TaskUnbindFromProjectRequest("wrongToken", null, null))
        );
        @NotNull final String testProjectName = "UnitTestProject_" + UUID.randomUUID().toString();
        @NotNull final String testTaskName = "UnitTestTask_" + UUID.randomUUID().toString();
        @Nullable ProjectCreateResponse projectResponse = projectEndpoint.createProject(
                new ProjectCreateRequest(userToken, testProjectName, "desc")
        );
        Assert.assertNotNull(projectResponse);
        Assert.assertNotNull(projectResponse.getProject());
        @NotNull final String projectId = projectResponse.getProject().getId();
        @Nullable TaskCreateResponse taskResponse = taskEndpoint.createTask(
                new TaskCreateRequest(userToken, testTaskName, "desc")
        );
        Assert.assertNotNull(taskResponse);
        Assert.assertNotNull(taskResponse.getTask());
        @NotNull final String taskId = taskResponse.getTask().getId();
        @Nullable TaskBindToProjectResponse bindResponse = taskEndpoint.bindTaskToProject(
                new TaskBindToProjectRequest(userToken, projectId, taskId)
        );
        Assert.assertNotNull(bindResponse);
        @Nullable TaskShowByIdResponse getResponse = taskEndpoint.showTaskById(
                new TaskShowByIdRequest(userToken, taskId)
        );
        Assert.assertNotNull(getResponse);
        Assert.assertNotNull(getResponse.getTask());
        Assert.assertEquals(projectResponse.getProject().getId(), getResponse.getTask().getProjectId());
        @Nullable TaskUnbindFromProjectResponse unbindResponse = taskEndpoint.unbindTaskFromProject(
                new TaskUnbindFromProjectRequest(userToken, projectId, taskId)
        );
        Assert.assertNotNull(unbindResponse);
        @Nullable TaskShowByIdResponse getUnbindResponse = taskEndpoint.showTaskById(
                new TaskShowByIdRequest(userToken, taskId)
        );
        Assert.assertNotNull(getUnbindResponse);
        Assert.assertNotNull(getUnbindResponse.getTask());
        Assert.assertNotEquals(projectResponse.getProject().getId(), getUnbindResponse.getTask().getProjectId());
        Assert.assertNull(getUnbindResponse.getTask().getProjectId());
    }

    @Test
    public void taskUpdateById() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(
                new TaskUpdateByIdRequest()
        ));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(
                new TaskUpdateByIdRequest(userToken, null, "1", "1")
        ));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(
                new TaskUpdateByIdRequest(null, null, null, null)
        ));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(
                new TaskUpdateByIdRequest("wrongToken", null, null, null)
        ));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.updateTaskById(
                new TaskUpdateByIdRequest(userToken, "wrongTaskId", null, null)
        ));
        @NotNull final String testTaskName = "UnitTestTask_" + UUID.randomUUID().toString();
        @Nullable TaskCreateResponse taskResponse = taskEndpoint.createTask(
                new TaskCreateRequest(userToken, testTaskName, "desc")
        );
        Assert.assertNotNull(taskResponse);
        Assert.assertNotNull(taskResponse.getTask());
        @Nullable TaskUpdateByIdResponse response = taskEndpoint.updateTaskById(
                new TaskUpdateByIdRequest(userToken, taskResponse.getTask().getId(), "test", "test")
        );
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTask());
        Assert.assertEquals(response.getTask().getId(), taskResponse.getTask().getId());
        Assert.assertNotEquals(response.getTask().getName(), taskResponse.getTask().getName());
        Assert.assertNotEquals(response.getTask().getDescription(), taskResponse.getTask().getDescription());
    }

    @Test
    public void taskGetById() {
        Assert.assertThrows(Exception.class, () -> taskEndpoint.showTaskById(
                new TaskShowByIdRequest()
        ));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.showTaskById(
                new TaskShowByIdRequest(userToken, "id")
        ));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.showTaskById(
                new TaskShowByIdRequest(null, null)
        ));
        Assert.assertThrows(Exception.class, () -> taskEndpoint.showTaskById(
                new TaskShowByIdRequest("wrongToken", null)
        ));
        @NotNull final String testTaskName = "UnitTestTask_" + UUID.randomUUID().toString();
        @Nullable TaskCreateResponse taskResponse = taskEndpoint.createTask(
                new TaskCreateRequest(userToken, testTaskName, "desc")
        );
        Assert.assertNotNull(taskResponse);
        Assert.assertNotNull(taskResponse.getTask());
        @Nullable TaskShowByIdResponse response = taskEndpoint.showTaskById(
                new TaskShowByIdRequest(userToken, taskResponse.getTask().getId())
        );
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTask());
        Assert.assertEquals(response.getTask().getId(), taskResponse.getTask().getId());
    }

}
