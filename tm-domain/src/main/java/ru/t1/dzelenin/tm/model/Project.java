package ru.t1.dzelenin.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.api.model.IWBS;
import ru.t1.dzelenin.tm.enumerated.Status;

import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
public final class Project extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @NotNull
    private Date created = new Date();

    @Nullable
    private Date dateBegin;

    @Nullable
    private Date dateEnd;

    public Project(@NotNull final String name, @NotNull final Status status, @NotNull final Date dateBegin) {
        this.name = name;
        this.status = status;
        this.dateBegin = dateBegin;
    }

}
