package ru.t1.dzelenin.tm.dto.response.domain;

import lombok.NoArgsConstructor;
import ru.t1.dzelenin.tm.dto.response.AbstractResponse;

@NoArgsConstructor
public final class DataBinaryLoadResponse extends AbstractResponse {
}
