package ru.t1.dzelenin.tm.dto.request.domain;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public final class DataJsonLoadJaxBRequest extends AbstractUserRequest {

    public DataJsonLoadJaxBRequest(@Nullable String token) {
        super(token);
    }

}
