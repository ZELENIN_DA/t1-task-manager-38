package ru.t1.dzelenin.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.dzelenin.tm.dto.response.AbstractResponse;
import ru.t1.dzelenin.tm.model.Task;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractTaskResponse extends AbstractResponse {

    @Nullable
    private Task task;

    public AbstractTaskResponse(@Nullable Task task) {
        this.task = task;
    }

}
