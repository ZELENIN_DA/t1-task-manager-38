CREATE TABLE IF NOT EXISTS taskmanager.project
(
    id character varying(50) COLLATE pg_catalog."default" NOT NULL,
    name character varying(100) COLLATE pg_catalog."default" NOT NULL,
    description character varying(500) COLLATE pg_catalog."default",
    status character varying(50) COLLATE pg_catalog."default",
    user_id character varying(50) COLLATE pg_catalog."default",
    date_begin timestamp without time zone,
    date_end timestamp without time zone,
    CONSTRAINT project_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS taskmanager.project
    OWNER to tm;